curl -X POST https://api.datacite.org/dois \
-H "Content-Type: application/vnd.api+json" \
-H "Authorization: Bearer YOUR_API_TOKEN" \
-d '{
  "data": {
    "type": "dois",
    "attributes": {
      "prefix": "10.1234",
      "creators": [{"name": "John Doe"}],
      "titles": [{"title": "My Research Paper"}],
      "publisher": "My University",
      "publicationYear": "2024",
      "types": {"resourceTypeGeneral": "Text", "resourceType": "JournalArticle"},
      "descriptions": [{"description": "An abstract of my research paper", "descriptionType": "Abstract"}],
      "url": "http://myuniversity.edu/research/my-research-paper"
    }
  }
}'

