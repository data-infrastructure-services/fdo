#!/bin/bash
# This script splits a text file into smaller files based on the "SECTION" keyword

input_file="mapping.js"
output_file_prefix="variable"

# Initialize variables
current_output_file=""
file_counter=1

# Read the input file line by line
while IFS= read -r line; do
    if [[ "$line" == *var* ]]; then
        # Close the current output file if it's open
        if [ -n "$current_output_file" ]; then
            exec 1>&6 6>&-
        fi

        # Create a new output file
        current_output_file="${output_file_prefix}_${file_counter}.txt"
        ((file_counter++))

        # Redirect output to the new file
        exec 6>&1 > "$current_output_file"

        # Print the current line to the new file
        echo "$line"
    else
        # Print the line to the current output file
        echo "$line"
    fi
done < "$input_file"

# Close the last output file if it's open
if [ -n "$current_output_file" ]; then
    exec 1>&6 6>&-
fi
